#!/bin/bash

TEMPORARY_HOSTS_BANNER="# this is a temporary generated file, will be deleted"

if [ ! -v DIR -o -z "$DIR" ]; then
    echo "DIR env variable not set or zero length"
    exit 1
fi

function generate_hosts_file() {
    if [ -e "$1" ]; then
        echo "$1 exists, not overwriting."
    fi

    if [ ! -v QUICKLUNCH_FQDN ]; then
        echo "Setting QUICKLUNCH_FQDN is mandatory."
        exit 1
    fi

    if [ "$QUICKLUNCH_FQDN" == "localhost" ]; then
        ANSIBLE_CONNECTION=local
    else
        ANSIBLE_CONNECTION=ssh
    fi

    if [ ! -v QUICKLUNCH_USER ]; then
        QUICKLUNCH_USER=root
    fi

    mkdir "$DIR/tmp" &> /dev/null || true
    echo "$TEMPORARY_HOSTS_BANNER" > "$1"
    echo "[quickstart_host_root]" >> "$1"
    echo "$QUICKLUNCH_FQDN  ansible_connection=$ANSIBLE_CONNECTION  ansible_user=root quicklunch_user=$QUICKLUNCH_USER" >> "$1"
    echo "[quickstart_host]" >> "$1"
    echo "$QUICKLUNCH_FQDN  ansible_connection=$ANSIBLE_CONNECTION  ansible_user=$QUICKLUNCH_USER quicklunch_user=$QUICKLUNCH_USER" >> "$1"
}

# removes hosts file if it contains $TEMPORARY_HOSTS_BANNER
# first argument is the hosts file to check and optionally remove
function remove_hosts_file_if_temporary() {
    if [ -z "$1" ]; then
        echo "get_hosts_file_path first argument has zero length"
        exit 1
    fi

    if grep "$TEMPORARY_HOSTS_BANNER" "$1" &> /dev/null; then
        rm "$1"
    fi
}
