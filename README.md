Quicklunch
==========

Usage
-----

First, set the machine you want Quicklunch to talk to:

    export QUICKLUNCH_FQDN=my-machine-fqdn.example.org

Then run the prep playbook, which will e.g. install qemu-ev for live
VM snapshots:

    QUICKLUNCH_PLAYBOOK=playbooks/quicklunch-prep.yml ./quicklunch.sh

**Reboot is required after qemu-ev installation.**

Then run `./quicklunch.sh`, passing it some variables and answer
files, which will set variables for Quickstart. Here are some
deployment examples:

* Deploy 3 controller + 3 compute from scratch, use slightly larger
  VMs than default, update container content to latest:

  ```bash
  ./quicklunch.sh \
       -e quickstart_cmd_release=tripleo-ci/CentOS-7/master.yml \
       -e quickstart_cmd_nodes=3ctlr_3comp.yml \
       -e @answers/general_config/containers-pcmk-notls.yml \
       -e @answers/playbook/new-upto-overcloud.yml \
       -e @answers/tweaks/bigger-vms.yml \
       -e @answers/tweaks/ntp-pool.yml \
       -e @answers/tweaks/host-dns.yml \
       -e @answers/tweaks/update-containers.yml \
  ```

* Deploy only overcloud (use existing undercloud) according to a
  definition from some featureset, e.g. featureset026 with Kubernetes,
  use hosts DNS servers, and THT from
  /home/stack/tripleo-heat-templates. Additionally override some
  features of featureset026 - don't use deployed server:

  ```bash
  ./quicklunch.sh \
      -e quickstart_cmd_release=tripleo-ci/CentOS-7/master.yml \
      -e quickstart_cmd_nodes=1ctlr_1comp.yml \
      -e @answers/playbook/overcloud.yml \
      -e @answers/general_config/featureset.yml \
      -e @answers/tweaks/ntp-pool.yml \
      -e @answers/tweaks/host-dns.yml \
      -e @answers/tweaks/custom-tht.yml \
      -e @answers/tweaks/validation-warnings-nonfatal.yml \
      -e featureset_number=026
  ```
