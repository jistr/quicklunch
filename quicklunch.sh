#!/bin/bash

set -euo pipefail

DIR=$(cd $(dirname "$BASH_SOURCE[0]") && pwd)
source "$DIR/lib/common.sh"

QUICKLUNCH_HOSTS=${QUICKLUNCH_HOSTS:-"$DIR/tmp/hosts.$QUICKLUNCH_FQDN"}
QUICKLUNCH_PLAYBOOK=${QUICKLUNCH_PLAYBOOK:-playbooks/quicklunch.yml}

remove_hosts_file_if_temporary "$QUICKLUNCH_HOSTS"
generate_hosts_file "$QUICKLUNCH_HOSTS"

export ANSIBLE_ROLES_PATH=${ANSIBLE_ROLES_PATH:-roles:../quicklunch-$(id -un)/roles}
ansible-playbook \
    -i "$QUICKLUNCH_HOSTS" \
    -e "quicklunch_local_dir=$DIR" \
    -e "@answers/_defaults.yml" \
    "$@" \
    "$QUICKLUNCH_PLAYBOOK"

remove_hosts_file_if_temporary "$QUICKLUNCH_HOSTS"
